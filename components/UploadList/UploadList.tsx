import React from 'react';
import formatBytes from '../../utils/formatBytes';
import  UploadItem from '../UploadItem';
import { UploadsConsumer } from '../../contexts/Uploads';

import styles from './upload-list.scss';

function UploadList() {
  return  (
    <UploadsConsumer>
      {
        (state : any) => (
          <div className={styles['upload-list']}>

            {
              state.uploadError &&
              <div className={styles['error-message']}>
                {state.uploadError}
                <button onClick={state.dismissUploadError}>x</button>
              </div>
            }

            <div className={styles['upload-list-header']}>
              <h1>
                {
                  state.fetchingUploads ?
                  'fetching uploads..' :
                  `${state.filteredUploads.length} documents`
                }
              </h1>
              <p>


                {
                  state.fetchingUploads ?
                  '' :
                  `Total size ${formatBytes(state.totalUploadSize)}`
                }
              </p>
            </div>

            <div className={styles['upload-list-grid']}>
              {
                state.filteredUploads.map((upload: any, i: number) => (
                  <UploadItem key={i} data={upload} deleteUpload={() => state.deleteUpload(upload.id)} />
                ))
              }
            </div>
          </div>
        )
      }
    </UploadsConsumer>
  )
}

export default UploadList;

import React from 'react';
import formatBytes from '../../utils/formatBytes';
import styles from './upload-item.scss';

function UploadItem(props : any) {
  const { data, deleteUpload } = props;
  const backgroundImage : string = `url(/uploads/${data.id})`;

  return (
    <div className={styles['upload-item']} style={{ backgroundImage }}>
      <div className={styles['upload-item__detail']}>
        <h3>{data.title}</h3>
        <p>{formatBytes(data.size)}</p>

        <button onClick={deleteUpload}>
          delete
        </button>
      </div>
    </div>
  );
}

export default UploadItem;

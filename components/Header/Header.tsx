import React from 'react';
import { UploadsConsumer } from '../../contexts/Uploads';

import styles from './header.scss';

function Header() {
  return (
    <UploadsConsumer>
      {
        (state: any) => (
          <header className={styles.header}>
            <div className={styles['header__container']}>
              <input
                type="text"
                placeholder="Search for files.."
                className={styles['header__search-input']}
                onChange={e => state.searchUploads(e.target.value)}
              />

              <div className={styles['file-input__container']}>
                <p>max size 10MB (png or jpg)</p>

                <label>
                  Upload Image
                  <input
                    type="file"
                    id="file-input"
                    className={styles['file-input']}
                    accept="image/png, image/jpeg"
                    multiple={false}
                    onChange={e => state.upload(e.target.files)}
                  />
                </label>
              </div>
            </div>
          </header>
        )
      }
    </UploadsConsumer>
  );
}

export default Header;

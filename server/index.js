const next = require('next');
const dev = process.env.NODE_DEV !== 'production';
const nextApp = next({ dev });

function initServer() {
  const express = require('express');
  const bodyParser = require('body-parser');
  const fs = require('fs');
  const multer = require('multer');
  const path = require('path');
  const helmet = require('helmet');
  const app = express();
  const handle = nextApp.getRequestHandler();
  const PORT = process.env.PORT || 3000;
  const uploadsJSONPath = path.resolve(__dirname, 'uploads.json');

  // set multer config
  const storage = multer.diskStorage({
    destination: (req, file, callback) => {
      callback(null, path.resolve(__dirname, 'public/uploads'));
    },
    filename: (req, file, callback) => {
      callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
  });
  const upload = multer({ storage });

  // middleware
  app.use(helmet());
  app.use(express.static(path.resolve(__dirname, 'public')));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  // api - upload handlers
  app.route('/api/uploads')
    // send uploads json
    .get((req, res, next) => {
      try {
        res.sendFile(uploadsJSONPath);
      } catch (err) {
        next(err);
      }
    })
    // post upload - save image to fs, update uploads json
    .post(upload.single('file'), (req, res, next) => {

      // load existing uploads json
      fs.readFile(uploadsJSONPath, 'utf8', (err, json) => {
        if (err) next(err);

        try {
          const uploads = JSON.parse(json);
          const { filename, originalname, size } = req.file

          // create new upload record
          const upload = {
            id: filename, // KLUDGE - generate unique id
            file: filename,
            title: req.file.originalname,
            uploaded_at: Date.now(),
            size: req.file.size,
          };

          // add to existing
          uploads.push(upload);

          // write to fs
          fs.writeFile(uploadsJSONPath, JSON.stringify(uploads), err => {
            if (err) {
              next(err);
            } else {
              return res.send('OK');
            }
          });
        } catch (error) {
          next(error);
        }
      });
    });

    // delete file with param id
    app.route('/api/uploads/:id')
      .delete((req, res, next) => {
        const deleteId = req.params.id;

        // load exisiting uploads json
        fs.readFile(uploadsJSONPath, 'utf8', (err, json) => {
          if (err) next(err);

          try {
            let pendingFile;
            const uploads = JSON.parse(json);

            // mock delete by filtering json
            const filteredUploads = uploads.filter(upload => {
              if (upload.id === deleteId) {
                pendingFile = upload;
                return false;
              }
              return upload;
            });

            // update uploads json
            fs.writeFile(uploadsJSONPath, JSON.stringify(filteredUploads), err => {
              if (err) {
                next(err);
              } else {
                deleteFile(deleteId);
              }
            });
          } catch (error) {
            next(error);
          }
        });

        // delete file from fs
        function deleteFile(file) {
          fs.unlink(path.resolve(__dirname, `public/uploads/${file}`), (err) => {
            if (err) next(err);
          });

          return res.send('OK');
        }
      });

  // pass to next
  app.get('*', (req,res) => handle(req,res));

  // server listen
  app.listen(PORT, err => {
      if (err) throw err;
      console.log(`ready at http://localhost:${PORT}`)
  })
}

// next boilerplate, start next and init the server
nextApp.prepare().then(initServer);

export type Upload = {
  file: string;
  id: string;
  size: number;
  title: string;
  uploaded_at: number;
};

export type UploadsState = {
  uploads: Upload[];
  fetchingUploads: boolean;
  filteredUploads: Upload[];
  filterString: string;
  totalUploadSize: number;
  fetchUploads: () => void;
  deleteUpload: (id: number) => Promise<void>;
  searchUploads: (query: string) => void;
  upload: (files: any) => Promise<void>;
  uploadError: string;
  dismissUploadError: () => void;
};

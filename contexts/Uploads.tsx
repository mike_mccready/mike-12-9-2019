import React, { createContext, useState, useEffect } from 'react';
import { Upload, UploadsState } from '../types';

// init uploads context
const UploadsContext = createContext({});
export const UploadsConsumer = UploadsContext.Consumer;

export function UploadsProvider(props: { children: JSX.Element[]}) {

  // initialize state
  const [uploads, setUploads] = useState([]);
  const [fetchingUploads, setFetchingUploads] = useState(true);
  const [filteredUploads, setFilteredUploads] = useState([]);
  const [filterString, setFilterString] = useState('');
  const [totalUploadSize, setTotalUploadSize] = useState(0);
  const [uploadError, setUploadError] = useState('');

  // set effects
  // fetch initial uploads
  useEffect(() => { fetchUploads() }, []);

  // on filteredUploads update, calc upload size
  useEffect(() => {
    calcTotalUploadSize();
  }, [filteredUploads]);

  // start file operations
  // fetch uploads
  async function fetchUploads() {
    try {
      const res = await fetch('/api/uploads');
      const uploads = await res.json();

      setUploads(uploads);
      setFilteredUploads(uploads);
      setFetchingUploads(false);
    } catch (err) {
      console.error(err);
      setUploadError('Error fetching uploads, please refresh.');
      setFetchingUploads(false);
    }
  }

  // post file
  async function upload(files: any) {
    const validationError = validateFile(files);
    if (validationError) return setUploadError(validationError);

    try {
      const formData = new FormData();
      formData.append('file', files[0]);

      await fetch(
        '/api/uploads',
        {
          method: 'POST',
          // headers: { "Content-Type": "You will perhaps need to define a content-type here" },
          body: formData
        })

      fetchUploads();
    } catch (err) {
      setUploadError('There was a problem with your upload, please try again.');
    }
  }

  // delete file
  async function deleteUpload(id: number) {
    try {
      await fetch(`/api/uploads/${id}`, {
        method: 'DELETE',
        // headers: {'content-type': 'application/json'},
      })

      fetchUploads();
    } catch (err) {
      console.error(err)
      setUploadError('Error deleting upload.');
    }
  }

  function searchUploads(query: string) {
    const filteredUploads = uploads.filter((upload : Upload) => {
      const title = upload.title.toLowerCase();
      return title.includes(query.toLowerCase());
    });

    setFilterString(query);
    setFilteredUploads(filteredUploads);
  }
  // end file operations


  // helper functions
  function calcTotalUploadSize() {
    const totalUploadSize = filteredUploads.reduce((total: any, current: any) => {
      return total + current.size;
    }, 0);

    setTotalUploadSize(totalUploadSize);
  }

  function validateFile(files: any) {
    if (files.length > 1) return 'Please upload one file at a time';
    const file = files[0];
    const maxFileKb = 10;
    const supportedTypes = ['image/png', 'image/jpeg', 'image/jpg'];
    if (!supportedTypes.includes(file.type)) return 'File type not supported. Please choose a jpg or png';

    const size = file.size / 1024 / 1024;
    return size > maxFileKb ? 'File is too large. Please reduce to 10MB' : false;
  }

  function dismissUploadError() {
    setUploadError('');
  }

  // set upload provider state
  const state: UploadsState = {
    uploads,
    fetchingUploads,
    filteredUploads,
    filterString,
    totalUploadSize,
    fetchUploads,
    deleteUpload,
    searchUploads,
    upload,
    uploadError,
    dismissUploadError
  }

  return (
    <UploadsContext.Provider value={state}>
      { props.children }
    </UploadsContext.Provider>
  );
}

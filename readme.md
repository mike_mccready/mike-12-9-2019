# Mike McCready

## Installation
git clone
```
git clone https://mike_mccready@bitbucket.org/mike_mccready/mike-12-9-2019.git
```

cd into directory
```
cd mike-12-9-2019/
```

Install packages
```
npm install
```

Run Project
```
npm run dev
```

[Open localhost](http://localhost:3000/)



work done after time -
[Expanded test coverage and security enhancements](https://bitbucket.org/mike_mccready/mike-12-9-2019/src/security-upgrades/)



## Security
### addressed
- client side upload validation - verify size, file type
- helmet middleware on server

### not yet addressed
- csrf token integration
- sanitize search/filter bar inputs
- server-side image validation

## Improvements
- Finish typing on the frontend
- Server and api test coverage
- Add a database for persisting file data
- Extend typescript config for server
- Upload multiple files
- Upload detail views (modal or direct to new page)
- Upload file metadata (title, description, uploader, etc)
- Upload modal with progress bar, image editing
- Drag and drop uploader
- Pagination
- Run search on Server
- Fuzzy search (with debounce)
- Image compression / optimization
- Persist files to somewhere other than disk storage (s3, ipfs)



## Libraries

External dependencies

- next - react ssr framework
- express - server implementation
- helmet - server secure middleware
- nodemon - live updates for server development
- multer - assist with file uploads
- node-sass, @zeit/next-sass - sass module styling
- jest, enzyme - testing suite
- identity-obj-proxy - necessary for running test suite with sass modules
- typescript - front-end typing
- ts-jest, @types, - types for installed packages


## API

### General observations
The api is built with Express. It includes boilerplate to handle next.js server-side rendering.
Currently images are saved to the file system and upload data is persisted to a JSON file.
Ideally the images would be hosted off-disk and upload data would be stored in a database.

### GET /api/uploads
- get an index of image uploads
- this endpoint returns the upload json data

### POST /api/uploads
- creates a new upload record and writes the image to the file system
- this endpoint returns 'OK' or an error

### DELETE /api/uploads/:id
- deletes the upload at the specified id
- accepts the upload id (to be deleted) as a route parameter
- this endpoint returns 'OK' or an error

## Other notes
This is a next.js app - pages are in the pages directory and component library is in components.
This app uses sass modules for styling and react hooks / context for state management.

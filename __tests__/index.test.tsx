/**
 * @jest-environment jsdom
 */
import React from 'react';
import ReactDOM from 'react-dom';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Index from '../pages/index';
import Header from '../components/Header';
import UploadList from '../components/UploadList';

configure({adapter: new Adapter()});

describe('Index', () => {
  it('should render without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Index />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should render header', () => {
    const wrapper = mount(<Index />);
    expect(wrapper.find(Header).length === 1);
    wrapper.unmount();
  });

  it('should render upload list', () => {
    const wrapper = mount(<Index />);
    expect(wrapper.find(UploadList).length === 1);
    wrapper.unmount();
  });
});

// afterEach(cleanup)
//
// it('Should upload a file', () => {
//   const { getByLabelText, getByText } = render(<Index />);
//   const fileInput = getByLabelText("Choose File");
//   const file = new File(["(⌐□_□)"], "chucknorris.png", {
//     type: "image/png"
//   });
//
//   Object.defineProperty(fileInput, "files", {
//     value: [file]
//   });
//
//   fireEvent.change(fileInput);
//
//
//  })


// it('button click changes props', () => {
//   const { getByText } = render(<App>
//                                 <TestHook />
//                                </App>)
//
//   expect(getByText(/Moe/i).textContent).toBe("Moe")
//
//   fireEvent.click(getByText("Change Name"))
//
//   expect(getByText(/Steve/i).textContent).toBe("Steve")
// })


// import React from 'react';
// import TestRenderer from 'react-test-renderer';
// import Index from '../pages/index';
// import Header from '../components/Header';
// import UploadList from '../components/UploadList';
//
// describe('Index', () => {
//   it('render a <Header /> component', () => {
//     const wrapper = TestRenderer.create(<Index />);
//     const instance = wrapper.root;
//     expect(instance.findByType(Header));
//   });
//
//   it('render a <UploadList /> component', () => {
//     const wrapper = TestRenderer.create(<Index />);
//     const instance = wrapper.root;
//     expect(instance.findByType(UploadList));
//   });
//
//   it('uploads a file', () => {
//     const wrapper = TestRenderer.create(<Index />);
//     const instance = wrapper.root;
//     const header = instance.findByType(Header);
//
//     header.find('input').simulate('change', {
//       target: { files: [ 'dummyValue.something' ] }
//     });
//   });
// });

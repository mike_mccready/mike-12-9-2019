// import { NextPage } from 'next';
import React from 'react';
import Header from '../components/Header';
import UploadList from '../components/UploadList';
import { UploadsProvider } from '../contexts/Uploads';
import styles from './index.scss';

function Home() {
  return (
    <UploadsProvider>
      <Header />
      <div className={styles.container}>
        <UploadList />
      </div>
    </UploadsProvider>
  );
}

export default Home;
